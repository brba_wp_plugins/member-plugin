<h1><?php esc_html_e( "Manage Member Jobs", "member-plugin"); ?></h1>


<?php

global $wpdb;
global $member_jobs_table;

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

if (isset($_GET['action'])) {
    if ($_GET['action'] == 'delete') { // delete event
        // get ID to delete
        $id = $_GET['id'];

        $wpdb->delete(
            $member_jobs_table,
            array(
                "id" => $id
            )
        );

    }


}


class MemberJobsTable extends WP_List_Table {

    function get_columns() {
        $columns = array(
            "member_id" => "Member",
            'company_name' => "Company Name",
            "job_location" => "Job Location",
            "job_description" => "Job Description",
            "street_address" => "Street Address",
            "company_website" => "Company Website"
        );

        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'company_name' => array('company_name', true),
            "job_location" => array('job_location', true),
            "job_description" => array('job_description', true),
            "street_address" => array('street_address', true),
            "company_website" => array('company_website', true),
            "member_id" => array("member_id", true)
        );

        return $sortable_columns;
    }

    function sort_reorder() {

        global $member_jobs_table;
        global $wpdb;

        $orderby = ( ! empty($_GET['orderby']) ) ? $_GET['orderby'] : 'id';
        $order = ( ! empty($_GET['order']) ) ? $_GET['order'] : 'asc';

        return $wpdb->get_results("SELECT * FROM $member_jobs_table ORDER BY $orderby $order"); // SQL INJECTION?

    }

    function prepare_items() {

        global $wpdb;
        global $member_jobs_table;

        $columns = $this->get_columns();

        $hidden_vals = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden_vals, $sortable);

        // $this->items = $wpdb->get_results("SELECT * FROM $member_jobs_table");
        $this->items = $this->sort_reorder();
    }

    function column_default($item, $column_name) {

        // TODO made this just be $item[$column_name] unless it is $column_name is validated.

        switch ($column_name) {

        case "member_id":
                return get_member_from_id($item->member_id);
        case "company_name":
                return $item->company_name;
        case "job_location":
            return $item->job_location;
        case "job_description":
            return $item->job_description;
        case "street_address":
            return $item->street_address;
        case "company_website":
            return $item->company_website;
        default:
            return print_r( $item, true );

        }
    }

    function column_member_id($item) {

        $page = $_REQUEST['page'];

        $actions = array(
            "delete" => "<a href='?page=$page&action=delete&id=$item->id'>Delete</a>"
        );

        return sprintf("%s %s", get_member_from_id($item->member_id), $this->row_actions($actions));
    }


}

$membersListTable = new MemberJobsTable();
$membersListTable->prepare_items();
$membersListTable->display(); 
