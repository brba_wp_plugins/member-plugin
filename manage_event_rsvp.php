<h1><?php esc_html_e( "Manage Member Event RSVPs", "member-plugin"); ?></h1>


<?php

global $wpdb;
global $member_rsvp_table;

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

if (isset($_GET['action'])) {
    if ($_GET['action'] == 'delete') { // delete event
        // get ID to delete
        $id = $_GET['id'];

        $wpdb->delete(
            $member_rsvp_table,
            array(
                "id" => $id
            )
        );

    }


}


class MemberRSVPTable extends WP_List_Table {

    function get_columns() {
        $columns = array(
            "event_id" => "Event",
            'name' => "Name",
            "phone" => "Phone",
            "email" => "E-Mail",
            "attending" => "Attending"
        );

        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'event_id' => array('event_id', true),
            "name" => array('name', true),
            "phone" => array('phone', false),
            "email" => array('email', true),
            "attending" => array('attending', true),
        );

        return $sortable_columns;
    }

    function sort_reorder() {

        global $member_rsvp_table;
        global $wpdb;

        $orderby = ( ! empty($_GET['orderby']) ) ? $_GET['orderby'] : 'id';
        $order = ( ! empty($_GET['order']) ) ? $_GET['order'] : 'asc';

        return $wpdb->get_results("SELECT * FROM $member_rsvp_table ORDER BY $orderby $order"); // SQL INJECTION?

    }

    function prepare_items() {

        global $wpdb;
        global $member_rsvp_table;

        $columns = $this->get_columns();

        $hidden_vals = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden_vals, $sortable);

        $this->items = $this->sort_reorder();
    }

    function column_default($item, $column_name) {

        // TODO made this just be $item[$column_name] unless it is $column_name is validated.

        switch ($column_name) {

        case "event_id":
                return get_event_from_id($item->event_id);
        case "name":
                return $item->name;
        case "phone":
            return $item->phone;
        case "email":
            return $item->email;
        case "attending":
            return $item->attending ? "Yes" : "No";
        default:
            return print_r( $item, true );

        }
    }

    function column_event_id($item) {

        $page = $_REQUEST['page'];

        $actions = array(
            "delete" => "<a href='?page=$page&action=delete&id=$item->id'>Delete</a>"
        );

        return sprintf("%s %s", $this->get_event_from_id($item->event_id), $this->row_actions($actions));
    }

    function get_event_from_id($event_id) {

        global $member_events_table_name;
        global $wpdb;

        $event = $wpdb->get_row("SELECT * FROM $member_events_table_name WHERE id=$event_id");
        return $event->event_name;



    }


}

$membersListTable = new MemberRSVPTable();
$membersListTable->prepare_items();
$membersListTable->display(); 
