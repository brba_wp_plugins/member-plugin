<h1><?php esc_html_e( "Manage Member Users", "member-plugin"); ?></h1>

<p>Add a member user:</p>

<form method="POST">
    <input type="hidden" name="add_member_user" value="true">

    User: <?php wp_dropdown_users() ?>

    <?php make_brba_members_dropdown() ?>

    <input type="submit" value="Add">

</form>


<?php

global $wpdb;
global $member_users_table;

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

if (isset($_GET['action'])) {
    if ($_GET['action'] == 'delete') { // delete event
        // get ID to delete
        $id = $_GET['id'];

        $wpdb->delete(
            $member_users_table,
            array(
                "id" => $id
            )
        );

    }


}


class MemberUsersTable extends WP_List_Table {

    function get_columns() {
        $columns = array(
            "user_id" => "User",
            "member_id" => "Member"
        );

        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            "user_id" => array("user_id", true),
            "member_id" => array("member_id", true)
        );

        return $sortable_columns;
    }

    function sort_reorder() {

        global $member_users_table;
        global $wpdb;

        $orderby = ( ! empty($_GET['orderby']) ) ? $_GET['orderby'] : 'user_id';
        $order = ( ! empty($_GET['order']) ) ? $_GET['order'] : 'asc';

        return $wpdb->get_results("SELECT * FROM $member_users_table ORDER BY $orderby $order"); // SQL INJECTION?

    }

    function prepare_items() {

        global $wpdb;
        global $member_users_table;

        $columns = $this->get_columns();

        $hidden_vals = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden_vals, $sortable);

        // $this->items = $wpdb->get_results("SELECT * FROM $member_users_table");
        $this->items = $this->sort_reorder();
    }

    function column_default($item, $column_name) {

        // TODO made this just be $item[$column_name] unless it is $column_name is validated.

        switch ($column_name) {
            case "member_id":
                return get_member_from_id($item->member_id);
            case "user_id":
                $userdata = get_userdata($item->user_id);
                return $userdata->user_login;

            default:
                return print_r( $item, true );


        }
    }

    function column_member_id($item) {

        $page = $_REQUEST['page'];

        $actions = array(
            "delete" => "<a href='?page=$page&action=delete&id=$item->id'>Delete</a>"
        );

        return sprintf("%s %s", get_member_from_id($item->member_id), $this->row_actions($actions));
    }


}

$membersListTable = new MemberUsersTable();
$membersListTable->prepare_items();
$membersListTable->display(); 
