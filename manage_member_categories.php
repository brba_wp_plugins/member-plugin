<?php

if ($_POST['form'] == "true") {



    global $wpdb;
    global $member_table_name;

    $member_id = $_POST["member"];

    // First get the current categories, so we can modify them
    $categories_string = $wpdb->get_row("SELECT categories FROM $member_table_name WHERE id=$member_id");

    // Explode the categories to add another one
    $categories = explode(",", $categories_string->categories);

    if (sizeof($categories) == 1 && $categories[0] == "") {
        $categories[0] = sanitize_text_field($_POST["category"]);
    }

    else {
        array_push($categories, sanitize_text_field($_POST["category"]));
    }

    // Turn array comma-separated-category list

    $category_string = "";

    // Only add commas if size over 1
    if (sizeof($categories) > 1) {

        for ($i = 0; $i < sizeof($categories); $i++) {

            $category_string .= $categories[$i];

            if ($i < (sizeof($categories) -1)) {
                $category_string .= ",";
            }

        }

    }

    else {
        $category_string = $categories[0];
    }

    echo "<h2>$category_string</h2>";

    // Insert into the database

    $wpdb->update(
        $member_table_name,
        array(
            "categories" => $category_string
        ),
        array(
            "id" => $member_id
        ),
        array(
            "%s"
        ),
        array(
            "%d"
        )
    );





}


if ($_GET['action'] == "delete") {

    $member_id = $_GET["member"];
    $value = $_GET["value"];

    global $wpdb;
    global $member_table_name;

    $categories_string = $wpdb->get_row("SELECT categories from $member_table_name where id=$member_id");
    $categories = explode(",", $categories_string->categories);

    if ($categories[0] != "") {

        unset($categories[$value]);

        $category_string = "";

        // :clap: too lazy so I copied the code

        if (sizeof($categories) > 1) {

            for ($i = 0; $i < sizeof($categories); $i++) {

                $category_string .= $categories[$i];

                if ($i < (sizeof($categories) -1)) {
                    $category_string .= ",";
                }

            }

        }

        else {
            $category_string = $categories[0];
        }

        $wpdb->update(
            $member_table_name,
            array("categories" => $category_string),
            array("id" => $member_id),
            array("%s"),
            array("%d")
        );

        $page = $_REQUEST["page"];

        // Very hacky
        echo "<script>location.replace('?page=$page')</script>";


    }


}

?>


<h1>Manage Member Categories</h1>

<form method="POST">

    <input type="hidden" name="form" value="true">

    Choose a Member: <?=get_user_members_dropdown()?> <br>

    Category Name: <input type="text" name="category" placeholder="Your Category Here..."><br>

    <input type="submit" value="Add">

</form>

<style>

 #company-category-table > table, th, td {

     border: 1px solid black;
     padding: 10px;

 }

 #company-category-table {
     border-collapse: collapse;
 }


</style>


<table id="company-category-table">
    <tr>
        <th>Company Name</th>
        <th>Categories</th>
    </tr>

    <?php

    global $wpdb;
    global $member_table_name;

    $ids = get_user_members();

    $members = array();

    foreach($ids as $id) {
        $members_set = $wpdb->get_results("SELECT * FROM $member_table_name WHERE validated=1 AND id=$id");
        $members = array_merge($members, $members_set);
    }



    foreach ($members as $member) {

    ?>

        <tr>



        <td><?=$member->company_name?></td>

        <td>

            <ul>


            <?php

            $categories = explode(",", $member->categories);

            if (sizeof($categories) == 1 && $categories[0] == "") {
                $categories = array();
            }

            foreach ($categories as $index=>$category) {

                $page = $_REQUEST["page"];

                echo "<li>$category</li>";
                echo "<a href='?page=$page&action=delete&member=$member->id&value=$index'>Delete</a>";

            }

            ?>

            </ul>


        </td>

        <?php } ?>

        </tr>


</table>
