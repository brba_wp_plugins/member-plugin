<?php 
/*
 * Plugin Name: BRBA Member Plugin
 * Description: Allows people to create community events.
 * Version: 0.1
 * Author: Ramesh Balaji
 */


global $wpdb;

$member_table_name = $wpdb->prefix."brba_member";
$member_events_table_name = $wpdb->prefix."brba_member_events";
$member_users_table = $wpdb->prefix."brba_member_users";
$member_jobs_table = $wpdb->prefix."brba_member_jobs";
$member_rsvp_table = $wpdb->prefix."brba_member_event_rsvp";
$member_job_application_table = $wpdb->prefix."brba_member_job_application";

$current_db_ver = get_option("memberplugin_db_ver");
$this_db_ver = "0.8j";

if ($current_db_ver != $this_db_ver) {
    $charset_collate = $wpdb->get_charset_collate();
    $member_sql = "CREATE TABLE $member_table_name
            (id INT AUTO_INCREMENT NOT NULL,
             first_name VARCHAR(200) NOT NULL,
             last_name VARCHAR(200) NOT NULL,
             business_title VARCHAR(200) NOT NULL,
             company_name VARCHAR(200) NOT NULL,
             email_brba VARCHAR(200) NOT NULL,
             email_public VARCHAR(200) NOT NULL,
             address VARCHAR(200) NOT NULL,
             city VARCHAR(200) NOT NULL,
             state VARCHAR(200) NOT NULL,
             zip_code VARCHAR(200) NOT NULL,
             validated BOOL NOT NULL,
             address_public BOOL NOT NULL,
             website VARCHAR(200) NOT NULL,
             phone_number VARCHAR(200) NOT NULL,
             web_presence VARCHAR(200) NOT NULL,
             company_description VARCHAR(500) NOT NULL,
             primary_business_needs VARCHAR(500) NOT NULL,
             membership_takeaway VARCHAR(500) NOT NULL,
             brba_hear_about VARCHAR(500) NOT NULL,
             photo VARCHAR(200) NOT NULL,
             logo VARCHAR(200) NOT NULL,
             categories VARCHAR(2000)
            ) $charset_collate";

    $member_events_sql = "CREATE TABLE $member_events_table_name
            (id INT AUTO_INCREMENT NOT NULL,
             member_id INT NOT NULL,
             event_name VARCHAR(200) NOT NULL,
             category VARCHAR(200) NOT NULL,
             time DATETIME NOT NULL,
             location VARCHAR(200) NOT NULL,
             description TEXT
           ) $charset_collate";

    $member_users_sql = "CREATE TABLE $member_users_table
                         (id INT AUTO_INCREMENT NOT NULL,
                          user_id INT NOT NULL,
                          member_id INT NOT NULL) $charset_collate";


    $member_jobs_sql = "CREATE TABLE $member_jobs_table
                       (id int  auto_increment not null,
                        member_id int not null,
                        job_title varchar(200) not null,
                        company_name varchar(200) not null,
                        job_location varchar(200) not null,
                        job_description text not null,
                        street_address varchar(200) not null,
                        company_website varchar(200))
                       $charset_collate";

    $member_rsvp_sql = "CREATE TABLE $member_rsvp_table
                        (id INT auto_increment not null,
                         event_id INT not null,
                         name VARCHAR(200) not null,
                         phone VARCHAR(200) not null,
                         email VARCHAR(200) not null,
                         attending BOOL not null) $charset_collate";

    $member_job_application_sql = "CREATE TABLE $member_job_application_table
                                     (id INT auto_increment NOT NULL,
                                      job_id INT not null,
                                      name VARCHAR(200) not null,
                                      phone VARCHAR(200) not null,
                                      email VARCHAR(200) not null,
                                      why VARCHAR(2000) not null
                                     ) $charset_collate";

// Execute the query
    require_once(ABSPATH . "wp-admin/includes/upgrade.php");

    dbDelta($member_sql);
    dbDelta($member_events_sql);
    dbDelta($member_users_sql);
    dbDelta($member_jobs_sql);
    dbDelta($member_rsvp_sql);
    dbDelta($member_job_application_sql);

    // update db version
    update_option("memberplugin_db_ver", $db_ver);
}


function member_application_shortcode() {

    if (isset($_POST['member_app_form'])) {

        $data = $_POST;

        // Perform form validation
        foreach ($data as $value) {
            if (!isset($data)) {
                // TODO Do not continue. For now we will let it continue
            }
        }
        // Process our data
        unset($data["member_app_form"]);
        $data['validated'] = false;

        // Insert our data into the database
        global $wpdb;
        global $member_table_name;

        $wpdb->insert(
            $member_table_name,
            $data
        );

    }

    ob_start();
    include "member_application.php";
    return ob_get_clean();

}


add_shortcode("member_application", "member_application_shortcode");
add_shortcode("member_event_creation", "member_event_creation_shortcode");



function add_member_approval_page() {
    add_menu_page('Members', 'Members', 'manage_options', 'members_manage', 'manage_members' );
    add_submenu_page('members_manage', 'Manage Members', 'Manage Members', 'manage_options', 'members_manage' );
    add_submenu_page('members_manage', 'Manage Member Events', 'Manage Member Events', 'manage_options', 'manage_member_events', 'manage_member_events' );
    add_submenu_page('members_manage', 'Manage Member Users', 'Manage Member User', 'manage_options', 'manage_member_users', 'manage_member_users' );
    add_submenu_page('members_manage', 'Manage Member Jobs', 'Manage Member Jobs', 'manage_options', 'manage_member_jobs', 'manage_member_jobs' );
    add_submenu_page('members_manage', 'Manage Member Event RSVPs', 'Manage Member Event RSVPs', 'manage_options', 'manage_event_rsvp', 'manage_event_rsvp');


}

function add_member_event_page() {
    // Add form for events

    add_menu_page('Add Member Events', 'Add Member Events', 'read', 'add_member_events_menu', 'add_member_events' );

}

function manage_member_jobs() {
    ob_start();
    include "manage_member_jobs.php";
    return ob_get_contents();
}

function add_jobadd_page() {
    // Add form for events
    add_menu_page('Add Job', 'Add Job', 'read', 'jobadd_menu', 'make_jobadd_page' );

}

function make_jobadd_page() {
    if (isset($_POST['jobadd_page'])) {
        if ($_POST['jobadd_page'] == "true") {

            // insert data into database
            global $wpdb;
            global $member_jobs_table;

            // sanitize the $_POST array

            unset($_POST['jobadd_page']);
            unset($_POST['submit']);

            $_POST['member_id'] = $_POST['member'];
            $_POST['job_description'] = stripslashes($_POST['job_description']);


            unset($_POST['member']);

            // very hacky

            $wpdb->insert(
                $member_jobs_table,
                $_POST
            );

        }
    }
    ob_start();
    include 'member_jobadd_form.php';
    return ob_get_contents();
}

// Admin section
add_action("admin_menu", "add_member_approval_page");

// User section
add_action("admin_menu", "add_member_event_page");

// Jobadd section
add_action("admin_menu", "add_jobadd_page");

// Category manage section
add_action("admin_menu", "add_member_category_page");

// Job application manage
add_action("admin_menu", "add_job_application_page");

function add_job_application_page() {
    add_menu_page('Manage Job Applications', 'Manage Job Applications', 'read', 'add_member_job_applications_menu', 'add_job_applications_menu' );
}

function add_job_applications_menu() {
    ob_start();
    include "manage_job_applications.php";
    return ob_get_contents();
}


function add_member_category_page() {
    add_menu_page('Manage Member Categories', 'Manage Member Categories', 'read', 'add_member_categories_menu', 'add_member_categories' );
}

function add_member_categories() {
    ob_start();
    include "manage_member_categories.php";
    return ob_get_contents();
}

function manage_members() {
    ob_start();
    include 'manage_members.php';
    return ob_get_contents();
}

function manage_member_events() {
    ob_start();
    include 'manage_member_events.php';
    return ob_get_contents();
}

function manage_event_rsvp() {
    ob_start();
    include 'manage_event_rsvp.php';
    return ob_get_contents();
}

function add_member_events() {

    if (isset($_POST["add_member_event_form"])) {
        if ($_POST["add_member_event_form"] == "true") {
            // Get data
            $member = $_POST['member'];
            $name = $_POST['title'];
            $category = $_POST['category'];

            $date = $_POST["date"];
            $time = $_POST["time"];
            $datetime = "$date $time";

            $location = $_POST["location"];
            $description = stripslashes($_POST["description"]);

            $data = array();
            $data['member_id'] = $member;
            $data['event_name'] = $name;
            $data['category'] = $category;
            $data['time'] = $datetime;
            $data['location'] = $location;
            $data['description'] = $description;  // TODO Fix tinyMCE editor

            // Insert data

            global $wpdb;
            global $member_events_table_name;

            $wpdb->insert(
                $member_events_table_name,
                $data
            );


        }

    }

    ob_start();
    include 'add_member_events.php';
    return ob_get_contents();
}

function manage_member_users() {
    if (isset($_POST['add_member_user'])) {
        if ($_POST['add_member_user'] == "true") {
            // Form to add user was submitted

            // get data

            $user = $_POST['user'];
            $member = $_POST['member'];

            // insert into $member_users_table

            global $member_users_table;
            global $wpdb;
            $data = array("user_id" => $user, "member_id" => $member);

            // make sure there are no duplicates

            $row = $wpdb->get_row("SELECT * FROM $member_users_table WHERE user_id=$user AND member_id=$member");
            if (!isset($row)) {
                $wpdb->insert(
                    $member_users_table,
                    $data,
                    array(
                        "%d",
                        "%d",
                    )
                );

            }


        }

    }
    ob_start();
    include 'manage_member_users.php';
    return ob_get_contents();
}

// Helper functions

function make_brba_members_dropdown() {
    // Only validated members

    $dropdown_html = "<select name='member'>";

    global $wpdb;
    global $member_table_name;

    $results = $wpdb->get_results("SELECT * FROM $member_table_name WHERE validated=1");

    foreach ($results as $member) {
        $dropdown_html .= "<option value='$member->id'>$member->company_name</option>";
    }

    $dropdown_html .= "</select>";
    echo $dropdown_html;
    return $dropdown_html;
}

function get_user_members() {
    global $wpdb;
    global $member_users_table;

    $cuid = get_current_user_id();
    $results = $wpdb->get_results(
        "SELECT * FROM $member_users_table WHERE user_id=$cuid"
    );

    $members = array();

    foreach ($results as $member) {
        array_push($members, $member->member_id);
    }

    return $members;

}

function get_user_members_dropdown() {
    $members = get_user_members();

    $html = "<select name='member'>";

    foreach ($members as $member) {
        $name = get_member_from_id($member);
        $html .= "<option value='$member'>$name</option>";
    }

    $html .= "</select>";

    return $html;
}

function get_member_from_id($id) {
    global $wpdb;
    global $member_table_name;

    $result = $wpdb->get_row("SELECT * FROM $member_table_name WHERE id=$id");


    return $result->company_name;
}
