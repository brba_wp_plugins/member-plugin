<h1>Add Job</h1>

<form method="POST">

    <input name="jobadd_page" type="hidden" value="true">

    Choose Member: <?=get_user_members_dropdown() ?><br>
    Job Title: <input name="job_title" type="text"><br>
    Company Name: <input name="company_name" type="text"><br>
    Job Location: <input name="job_location" type="text"><br>
    Job Description: <?php wp_editor("", "job_description") ?><br>
    Street Address: <input name="street_address" type="text"><br>
    Company Website: <input name="company_website" type="text"><br>

    <?php submit_button("Post My Job") ?>

</form>
