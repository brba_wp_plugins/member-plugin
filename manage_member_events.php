<h1><?php esc_html_e( "Manage Members", "member-plugin"); ?></h1>

<?php

global $wpdb;
global $member_events_table_name;

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

if (isset($_GET['action'])) {
    if ($_GET['action'] == 'delete') { // delete event
        // get ID to delete
        $id = $_GET['id'];

        $wpdb->delete(
            $member_events_table_name,
            array(
                "id" => $id
            )
        );

    }

    elseif ($_GET['action'] == 'validate') {
        $id = $_GET['id'];

        $wpdb->update(
            $member_events_table_name,
            array( //update 
                "validated" => true
            ),
            array('id' => $id), // where
            array("%d"), // column value types
            array("%d") // where (id) type
        );

    }

    elseif ($_GET['action'] == 'invalidate') {
        $id = $_GET['id'];

        $wpdb->update(
            $member_events_table_name,
            array( //update
                "validated" => false
            ),
            array('id' => $id), // where
            array("%d"), // column value types
            array("%d") // where (id) type
        );

    }

}


class MemberEventsTable extends WP_List_Table {

    function get_columns() {
        $columns = array(
            "member_id" => "Member",
            "event_name" => "Event Name",
            "category" => "Category",
            "time" => "Time",
            "location" => "Location",
            "description" => "Description",
        );

        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            "member_id" => array("member_id", true),
            "event_name" => array("event_name", true),
            "category" => array("category", true),
            "time" => array("time", true),
            "location" => array("location", true),
            "description" => array("description", true),
        );

        return $sortable_columns;
    }

    function sort_reorder() {

        global $member_events_table_name;
        global $wpdb;

        $orderby = ( ! empty($_GET['orderby']) ) ? $_GET['orderby'] : 'member_id';
        $order = ( ! empty($_GET['order']) ) ? $_GET['order'] : 'asc';

        return $wpdb->get_results("SELECT * FROM $member_events_table_name ORDER BY $orderby $order"); // SQL INJECTION?

    }

    function prepare_items() {

        global $wpdb;
        global $member_events_table_name;

        $columns = $this->get_columns();

        $hidden_vals = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden_vals, $sortable);

        // $this->items = $wpdb->get_results("SELECT * FROM $member_events_table_name");
        $this->items = $this->sort_reorder();
    }

    function column_default($item, $column_name) {

        // TODO made this just be $item[$column_name] unless it is $column_name is validated.
 
        switch ($column_name) {
        case "member_id":
            return get_member_from_id($item->member_id);
        case "event_name":
            return $item->event_name;
        case "category":
            return $item->category;
        case "time":
            return $item->time;
        case "location":
            return $item->location;
        case "description":
            return $item->description;
        default:
            return print_r( $item, true );


        }
    }

    function column_member_id($item) {

        $page = $_REQUEST['page'];

        $actions = array(
            "delete" => "<a href='?page=$page&action=delete&id=$item->id'>Delete</a>"
        );

        return sprintf("%s %s", get_member_from_id($item->member_id), $this->row_actions($actions));
    }


}

$membersListTable = new MemberEventsTable();
$membersListTable->prepare_items();
$membersListTable->display(); 
