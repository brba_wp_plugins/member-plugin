<h1><?php esc_html_e( "Manage Job Applications", "member-plugin"); ?></h1>


<?php

global $wpdb;
global $member_job_application_table;

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

if (isset($_GET['action'])) {
    if ($_GET['action'] == 'delete') { // delete event
        // get ID to delete
        $id = $_GET['id'];

        $wpdb->delete(
            $member_rsvp_table,
            array(
                "id" => $id
            )
        );

    }
}




class MemberJobApplicationTable extends WP_List_Table {

    function get_columns() {
        $columns = array(
            "job_id" => "Job",
            'name' => "Name",
            "phone" => "Phone",
            "email" => "E-Mail",
            "why" => "Why Applicant Wants Job"
        );

        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'job_id' => array('job_id', true),
            "name" => array('name', true),
            "phone" => array('phone', false),
            "email" => array('email', true),
            "attending" => array('attending', true),
        );

        return $sortable_columns;
    }

    function sort_reorder() {

        global $member_job_application_table;
        global $member_jobs_table;
        global $wpdb;

        $orderby = ( ! empty($_GET['orderby']) ) ? $_GET['orderby'] : 'id';
        $order = ( ! empty($_GET['order']) ) ? $_GET['order'] : 'asc';

        $ids = get_user_members();

        $job_ids = array();
        foreach ($ids as $id) {
            $job_ids_iter = $wpdb->get_results("SELECT id FROM $member_jobs_table WHERE member_id=$id");

            foreach ($job_ids_iter as $job_id)  {
                array_push($job_ids, $job_id->id);
            }

        }



        $results = array();

        foreach ($job_ids as $id) {
            $results = array_merge($results, $wpdb->get_results("SELECT * FROM $member_job_application_table WHERE job_id=$id ORDER BY $orderby $order" )); // SQL INJECTION?
        }

        return $results;

    }

    function prepare_items() {

        global $wpdb;
        global $member_rsvp_table;

        $columns = $this->get_columns();

        $hidden_vals = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden_vals, $sortable);

        $this->items = $this->sort_reorder();
    }

    function column_default($item, $column_name) {

        // TODO made this just be $item[$column_name] unless it is $column_name is validated.

        switch ($column_name) {

        case "job_id":
                return $this->get_job_from_id($item->job_id);
        case "name":
                return $item->name;
        case "phone":
            return $item->phone;
        case "email":
            return $item->email;
        case "why":
            return $item->why;
        default:
            return print_r( $item, true );

        }
    }

    function column_job_id($item) {

        $page = $_REQUEST['page'];

        $actions = array(
            "delete" => "<a href='?page=$page&action=delete&id=$item->id'>Delete</a>"
        );

        return sprintf("%s %s", $this->get_job_from_id($item->job_id), $this->row_actions($actions));
    }

    function get_job_from_id($event_id) {

        global $member_jobs_table;
        global $wpdb;

        $job = $wpdb->get_row("SELECT * FROM $member_jobs_table WHERE id=$event_id");
        return $job->job_title;



    }


}

$membersListTable = new MemberJobApplicationTable();
$membersListTable->prepare_items();
$membersListTable->display(); 
