#!/bin/bash

zip membership-brba.zip *
scp membership-brba.zip 192.168.1.82:~/
ssh 192.168.1.82 "sudo -u www-data wp --path=/var/www/wordpress plugin install --force ~/membership-brba.zip"
rm  membership-brba.zip
