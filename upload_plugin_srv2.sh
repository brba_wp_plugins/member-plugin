#!/bin/bash

zip membership-brba.zip *
scp membership-brba.zip brba-demo.now.im:~/
ssh brba-demo.now.im "sudo -u www-data wp --path=/var/www/wordpress plugin install --force ~/membership-brba.zip"
rm  membership-brba.zip
