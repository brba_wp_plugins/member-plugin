<h1><?php esc_html_e( "Manage Members", "member-plugin"); ?></h1>

<?php

global $wpdb;
global $member_table_name;

function send_renewal_email($company_email) {
    wp_mail($company_email, "Renew your BRBA membership", "Please log in to the site to update renew your membership.");
}

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

if (isset($_GET['action'])) {
    if ($_GET['action'] == 'delete') { // delete event
        // get ID to delete
        $id = $_GET['id'];

        $wpdb->delete(
            $member_table_name,
            array(
                "id" => $id
            )
        );

    }

    elseif ($_GET['action'] == 'validate') {
        $id = $_GET['id'];

        $wpdb->update(
            $member_table_name,
            array( //update 
                "validated" => true
            ),
            array('id' => $id), // where
            array("%d"), // column value types
            array("%d") // where (id) type
        );

        $member = $wpdb->get_row("SELECT * FROM $member_table_name WHERE id=$id");

        wp_schedule_single_event( time() + 120, 'send_renewal_email', array($member->email_brba) );



    }

    elseif ($_GET['action'] == 'invalidate') {
        $id = $_GET['id'];

        $wpdb->update(
            $member_table_name,
            array( //update
                "validated" => false
            ),
            array('id' => $id), // where
            array("%d"), // column value types
            array("%d") // where (id) type
        );

    }

}


class MembersListTable extends WP_List_Table {

    function get_columns() {
        $columns = array(
            "first_name" => "First Name",
            "last_name" => "Last Name",
            "business_title" => "Business Title",
            "company_name" => "Company Name",
            "email_brba" => "BRBA Email",
            "email_public" => "Public Email",
            "address" => "Address",
            "city" => "City",
            "state" => "State",
            "zip_code" => "ZIP Code",
            "validate" => "Validated",

        );

        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            "first_name" => array("title", false),
            "last_name" => array("description", false),
            "business_title" => array("description", false),
            "company_name" => array("description", false),
            "email_brba" => array("description", false),
            "public_brba" => array("description", false),
            "address" => array("description", false),
            "city" => array("description", false),
            "state" => array("description", false),
            "zip_code" => array("description", false),
            "validated" => array("description", false)

        );

        return $sortable_columns;
    }

    function sort_reorder() {

        global $member_table_name;
        global $wpdb;

        $orderby = ( ! empty($_GET['orderby']) ) ? $_GET['orderby'] : 'first_name';
        $order = ( ! empty($_GET['order']) ) ? $_GET['order'] : 'asc';

        return $wpdb->get_results("SELECT * FROM $member_table_name ORDER BY $orderby $order"); // SQL INJECTION?

    }

    function prepare_items() {

        global $wpdb;
        global $member_table_name;

        $columns = $this->get_columns();

        $hidden_vals = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden_vals, $sortable);

        // $this->items = $wpdb->get_results("SELECT * FROM $member_table_name");
        $this->items = $this->sort_reorder();
    }

    function column_default($item, $column_name) {

        // TODO made this just be $item[$column_name] unless it is $column_name is validated.
 
        switch ($column_name) {
        case "first_name":
            // TODO Make display of this bigger
            return $item->first_name;
        case "last_name":
            return $item->last_name;
        case "business_title":
            return $item->business_title;
        case "company_name":
            return $item->company_name;
        case "email_brba":
            return $item->email_brba;
        case "email_public":
            return $item->email_public;
        case "address":
            return $item->address;
        case "city":
            return $item->city;
        case "state":
            return $item->state;
        case "zip_code":
            return $item->zip_code;
        case "validate":
            $page = $_REQUEST['page'];
            if (!$item->validated) {
                return "<a href='?page=$page&action=validate&id=$item->id'>
                          <button class='button button-primary'>Validate</button>
                       </a>";
            }
            else {
                return "<a href='?page=$page&action=invalidate&id=$item->id'><button class='button button-primary'>Invalidate</button></a>";
            }

        default:
            return print_r( $item, true );


        }
    }

    function column_first_name($item) {

        $page = $_REQUEST['page'];

        $actions = array(
            "delete" => "<a href='?page=$page&action=delete&id=$item->id'>Delete</a>"
        );

        return sprintf("%s %s", $item->first_name, $this->row_actions($actions));
    }


}

$membersListTable = new MembersListTable();
$membersListTable->prepare_items();
$membersListTable->display(); 
